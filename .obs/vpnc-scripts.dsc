Format: 1.0
Source: vpnc-scripts
Binary: vpnc-scripts
Architecture: all
Version: 0.1~git20220305
Maintainer: OpenConnect Team <openconnect-devel@lists.infradead.org>
Homepage: https://www.infradead.org/openconnect/vpnc-script.html
Standards-Version: 4.6.0
Vcs-Browser: https://gitlab.com/openconnect/vpnc-scripts
Vcs-Git: https://gitlab.com/openconnect/vpnc-scripts.git
Build-Depends: debhelper-compat (= 12)
Package-List:
 vpnc-scripts deb net optional arch=all
Checksums-Sha1:
 4ec0a3855a12e87879ee93ee18622f8d3ffe7dc8 39925 vpnc-scripts_0.1~git20210402.orig.tar.gz
Checksums-Sha256:
 cd00e831904554c7acbc9cd20e6457c6e787fe52dc2f75d39263a65faccc19f0 39925 vpnc-scripts_0.1~git20210402.orig.tar.gz
Files:
 0de3882a4daed875229e3f25c7789d40 39925 vpnc-scripts_0.1~git20210402.orig.tar.gz
